<?php
/**
 * The template for displaying pages
 *
 * This is the template that displays all pages by default.
 * Please note that this is the WordPress construct of pages and that
 * other "pages" on your WordPress site will use a different template.
 *
 * @package FoundationPress
 * @since FoundationPress 1.0.0
 */

 get_header(); ?>
 

<div id="page" role="main">
   <header>
      <h1 class="entry-title"><?php the_title(); ?></h1>
   </header>
   <div class="row">
      <div class="small-12 medium-3 columns">
         <div class="sub-featured-img">
            <?php
               if (types_render_field('video-url', array('output'=>'true'))) {
                  echo types_render_field('video-url');
               } elseif ( has_post_thumbnail() ) { 
                  the_post_thumbnail();
               } else {

               }
            ?>
         </div>
         <?php get_sidebar(); ?>
      </div>
      <div class="small-12 medium-9 columns">
         <article <?php post_class('main-content') ?> id="post-<?php the_ID(); ?>">
            <div class="entry-content">
               <?php the_content(); ?>
               
               <!-- Contact Page -->
               <?php if ( is_page('contact-us') ) { ?>
                  <?php get_template_part( 'template-parts/contact-widgets' ); ?>

               <!-- Committees Page -->
               <?php } elseif ( is_page('committees') ) { ?>
                  <div class="row">
                     <div class="medium-6 columns">
                        <h4>Budgets</h4>
                        <ul>
                           <?php $budget = new WP_Query( array( 'post_type' => 'budget', 'posts_per_page' => -1 ) );
                           while( $budget->have_posts() ) : $budget->the_post(); ?>
                              
                                 <li>
                                 <a href="<?php echo types_render_field( "file", array( ) ) ?>" target="_blank">
                                 <?php echo types_render_field( "file-date", array( ) ) ?>
                                 </a>
                                 </li>
                              
                           <?php endwhile; wp_reset_query(); ?>
                        </ul>
                     </div>
                     <div class="medium-6 columns">
                        <h4>Audits</h4>
                        <ul>
                           <?php $audit = new WP_Query( array( 'post_type' => 'audit', 'posts_per_page' => -1 ) );
                           while( $audit->have_posts() ) : $audit->the_post(); ?>
                              
                                 <li>
                                 <a href="<?php echo types_render_field( "file", array( ) ) ?>" target="_blank">
                                 <?php echo types_render_field( "file-date", array( ) ) ?>
                                 </a>
                                 </li>
                              
                           <?php endwhile; wp_reset_query(); ?>
                        </ul>
                     </div>
                  </div>

               <!-- Board of Directors Page -->
               <?php } elseif ( is_page('board-of-directors') ) { ?>
                  <h4>Meeting Minutes</h4>
                  <?php $minutes = new WP_Query( array( 'post_type' => 'minutes', 'posts_per_page' => -1 ) );
                  while( $minutes->have_posts() ) : $minutes->the_post(); ?>
                     <ul>
                        <li>
                        <a href="<?php echo types_render_field( "file", array( ) ) ?>" target="_blank">
                           <?php echo types_render_field( "file-date", array( ) ) ?>
                        </a>
                        </li>
                     </ul>
                  <?php endwhile; wp_reset_query(); ?>

               <!-- Staff Page -->
               <?php } elseif ( is_page('staff') ) { ?>
                  <div class="row show-for-medium">
                     <div class="medium-3 columns">
                        <h4>Name</h4>
                     </div>
                     <div class="medium-6 columns">
                        <h4>Title/Division</h4>
                     </div>
                     <div class="medium-3 columns">
                        <h4>Email</h4>
                     </div>
                  </div>
                  <?php 
                  $i = 1;
                  $staff = new WP_Query( array( 'post_type' => 'staff-member', 'posts_per_page' => -1 ) );
                  while( $staff->have_posts() ) : $staff->the_post(); ?>
                     <div class="row">
                        <div class="medium-3 columns">
                           <?php echo types_render_field( "staff-name", array( ) ) ?>
                        </div>
                        <div class="medium-6 columns">
                           <?php echo types_render_field( "staff-title", array( ) ) ?>
                        </div>
                        <div class="medium-3 columns">
                           <?php echo types_render_field( "staff-email", array( ) ) ?>
                        </div>
                        <hr class="hide-for-medium">
                     </div>
                  <?php $i++; ?>
                  <?php endwhile; wp_reset_query(); ?>

               <!-- Community Planning Page -->
               <?php } elseif ( is_page('community-planning') || is_page('community-planning-archive') ) { ?>
                  <h3>Plan List</h3>
                  <ul>
                     <?php $plans = new WP_Query( array( 'post_type' => 'community-plan', 'posts_per_page' => -1 ) );
                     while( $plans->have_posts() ) : $plans->the_post(); ?>
                        
                           <li>
                           <a href="<?php echo types_render_field( "community-planning-file", array( ) ) ?>" target="_blank">
                              <?php echo types_render_field( "community-planning-title", array( ) ) ?>
                           </a>
                           </li>
                        
                     <?php endwhile; wp_reset_query(); ?>
                  </ul>
               <?php } elseif ( is_page(281) ) {
                  $args = array(
                     'post_type' => 'proposal-request',
                     'showposts' => -1
                  );
                  $proposals = new WP_Query( $args );

                  while( $proposals->have_posts() ) : $proposals->the_post(); 
               ?>

                     <ul class="accordion" data-accordion data-allow-all-closed="true">
                        <li class="accordion-item" data-accordion-item>
                           <a class="accordion-title">
                              <h4><?php the_title(); ?></h4>
                           </a>
                           <div class="accordion-content" data-tab-content>
                              <?php the_content(); ?>
                           </div>
                        </li>
                     </ul>

                     <?php endwhile; wp_reset_query(); ?>
               <?php } else { ?>

               <?php } ?>
            </div>
         </article>
      </div>
   </div>



 <?php do_action( 'foundationpress_after_content' ); ?>


 </div>

 <?php get_footer();