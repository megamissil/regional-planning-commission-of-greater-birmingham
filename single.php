<?php
/**
 * The template for displaying all single posts and attachments
 *
 * @package FoundationPress
 * @since FoundationPress 1.0.0
 */

get_header(); ?>
<div id="single-post" role="main">

<?php do_action( 'foundationpress_before_content' ); ?>
<?php while ( have_posts() ) : the_post(); ?>
	<article <?php post_class('main-content') ?> id="post-<?php the_ID(); ?>">
		<div class="row">
		    <div class="medium-3 columns">
		       	<div class="sub-featured-img">
		            <?php
		               if (types_render_field('video-url', array('output'=>'true'))) {
		                  echo types_render_field('video-url');
		               } elseif ( has_post_thumbnail() ) { 
		                  the_post_thumbnail();
		               } else {

		               }
		            ?>
		        </div>
		        <aside class="sidebar">
		         	<div class="side-box">
						<h4>More About the RPC</h4>
						<ul>
							<li><a href="/history/">History of the RPCGB</a></li>
							<li><a href="/services/">Services</a></li>
							<li><a href="/board-of-directors/">Board of Directors</a></li>
							<li><a href="/committees/">Committees</a></li>
							<li><a href="/resources/">Resources</a></li>
							<li><a href="/faq/">FAQ</a></li>
							<li><a href="/doing-business/">Doing Business</a></li>
							<li><a href="/staff/">Staff</a></li>
							<li><a href="/connections/">Connections</a></li>
							<li><a href="/requests-for-proposals-qualifications/">Requests for Proposals/Qualifications</a></li>
						</ul>
		         	</div>
	         	</aside>
		    </div>
		    <div class="medium-9 columns">
				<header>
			      	<h1 class="entry-title"><?php the_title(); ?></h1>
			   	</header>
				<?php the_content(); ?>
			</div>
		</div>
		<footer>
			<?php wp_link_pages( array('before' => '<nav id="page-nav"><p>' . __( 'Pages:', 'foundationpress' ), 'after' => '</p></nav>' ) ); ?>
			<p><?php the_tags(); ?></p>
		</footer>
	</article>
<?php endwhile;?>

</div>
<?php get_footer();