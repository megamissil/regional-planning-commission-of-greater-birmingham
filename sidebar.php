<?php
/**
 * The sidebar containing the main widget area
 *
 * @package FoundationPress
 * @since FoundationPress 1.0.0
 */

?>
<aside class="sidebar">
	<div class="side-box">
		<h4>More About the RPC</h4>
		<?php 
			global $post; // Setup the global variable $post
			$parent_title = get_the_title($post->post_parent);

			if ( is_page() && $post->post_parent ) {
				// Make sure we are on a page and that the page is a parent.
				$kids = wp_list_pages( 'sort_column=menu_order&sort_order=asc&title_li=&child_of=' . $post->post_parent . '&echo=0' );	
			} else {
				$kids = wp_list_pages( 'sort_column=menu_order&sort_order=asc&title_li=&child_of=' . $post->ID . '&echo=0' );
			}
			if ( $kids ) {
				echo '<h5>';
					echo '<span>';
						echo $parent_title;
					echo '</span>';
				echo '</h5>';
				echo '<ul class="secondary">';
					echo $kids;
					if ( is_page('economic-development') ) {
						echo '<li>';
						echo '<a href="http://www.venturesouth.org/" target="_blank">Venture South</a>';
						echo '</li>';
					}
				echo '</ul>';
			}
		?>

	</div>
	<div class="side-box">
		<?php dynamic_sidebar( 'sidebar-widgets' ); ?>
	</div>
</aside>
