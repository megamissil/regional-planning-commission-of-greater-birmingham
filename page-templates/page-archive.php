<?php
/*
Template Name: Archive
*/

get_header(); ?>

<div id="page" role="main" class="archive">
   	<header>
      	<h1 class="entry-title"><?php the_title(); ?></h1>
   	</header>
   	<div class="row">
      	<div class="medium-3 columns">
         	<div class="sub-featured-img">
            <?php
               	if ( has_post_thumbnail() ) :
                  	the_post_thumbnail();
               	endif;
            ?>
         	</div>
			<aside class="sidebar">
	         	<div class="side-box">
					<h4>More About the RPC</h4>
					<ul>
						<li><a href="/history/">History of the RPCGB</a></li>
						<li><a href="/services/">Services</a></li>
						<li><a href="/board-of-directors/">Board of Directors</a></li>
						<li><a href="/committees/">Committees</a></li>
						<li><a href="/resources/">Resources</a></li>
						<li><a href="/faq/">FAQ</a></li>
						<li><a href="/doing-business/">Doing Business</a></li>
						<li><a href="/staff/">Staff</a></li>
						<li><a href="/connections/">Connections</a></li>
						<li><a href="/requests-for-proposals-qualifications/">Requests for Proposals/Qualifications</a></li>
					</ul>
	         	</div>
         	</aside>
      	</div>
    	<div class="medium-9 columns">
			<article class="main-content">
			<!-- News -->
			<?php if ( is_page('news') ) {
				$news = new WP_Query( array( 'posts_per_page' => -1 ) );
				while( $news->have_posts() ) : $news->the_post(); ?>
					<?php get_template_part( '/template-parts/content', get_post_format() ); ?>
				<?php endwhile; ?>

			<!-- Events -->
			<?php } elseif ( is_page('events') ) {
				$event = new WP_Query( array( 'post_type' => 'event', 'posts_per_page' => -1 ) );
	            while( $event->have_posts() ) : $event->the_post(); ?>
					<?php get_template_part( '/template-parts/content', get_post_format() ); ?>
				<?php endwhile; ?>

			<!-- Jobs -->
			<?php } elseif ( is_page('employment') ) {
				$jobs = new WP_Query( array( 'post_type' => 'jobs', 'posts_per_page' => -1 ) );
	            while( $jobs->have_posts() ) : $jobs->the_post(); ?>
					<?php get_template_part( '/template-parts/content', get_post_format() ); ?>
				<?php endwhile; ?>

			<?php } else {

			} ?>

				<?php /* Display navigation to next/previous pages when applicable */ ?>
				<?php if ( function_exists( 'foundationpress_pagination' ) ) { foundationpress_pagination(); } else if ( is_paged() ) { ?>
					<nav id="post-nav">
						<div class="post-previous"><?php next_posts_link( __( '&larr; Older posts', 'foundationpress' ) ); ?></div>
						<div class="post-next"><?php previous_posts_link( __( 'Newer posts &rarr;', 'foundationpress' ) ); ?></div>
					</nav>
				<?php } ?>

			</article>
		</div>
	</div>
</div>

<?php get_footer();