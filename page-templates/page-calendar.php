<?php
/*
Template Name: Calendar
*/

get_header(); ?>

<div id="page" role="main" class="archive">
   	<header>
      	<h1 class="entry-title"><?php the_title(); ?></h1>
   	</header>
   	<div class="row">
      	<div class="medium-3 columns">
			<aside class="sidebar">
				<div class="side-box">
					<?php dynamic_sidebar( 'calendar-widget' ); ?>
				</div>
	         	<div class="side-box">
					<h4>More About the RPC</h4>
					<ul>
						<li><a href="/history/">History of the RPCGB</a></li>
						<li><a href="/services/">Services</a></li>
						<li><a href="/board-of-directors/">Board of Directors</a></li>
						<li><a href="/committees/">Committees</a></li>
						<li><a href="/resources/">Resources</a></li>
						<li><a href="/faq/">FAQ</a></li>
						<li><a href="/doing-business/">Doing Business</a></li>
						<li><a href="/staff/">Staff</a></li>
						<li><a href="/connections/">Connections</a></li>
						<li><a href="/requests-for-proposals-qualifications/">Requests for Proposals/Qualifications</a></li>
					</ul>
	         	</div>
         	</aside>
      	</div>
    	<div class="medium-9 columns">
			<article <?php post_class('main-content') ?> id="post-<?php the_ID(); ?>">
				<?php 
					$contact = get_post( 50 );
        			$content = $contact->post_content;
					
					echo $content; 
				?>
				
					<?php if ( is_page(50) ) { ?>
						<section class="cal-section">
							<?php get_template_part( 'template-parts/calendar-categories' ); ?>
						</section>

					<?php } else { ?>
					
						<?php get_template_part( 'template-parts/mpo-categories' ); ?>
					
					<?php } ?>	
				
				
			</article>
		</div>
	</div>
</div>

<?php get_footer();