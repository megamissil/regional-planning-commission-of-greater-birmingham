<?php
/*
Template Name: Meeting Archive
*/

get_header(); ?>

<div id="page" role="main" class="archive">
   	<header>
      	<h1 class="entry-title"><?php the_title(); ?></h1>
   	</header>
   	<div class="row">
      	<div class="medium-3 columns">
         	<div class="sub-featured-img">
            <?php
               	if ( has_post_thumbnail() ) :
                  	the_post_thumbnail();
               	endif;
            ?>
         	</div>
			<aside class="sidebar">
	         	<div class="side-box">
					<h4>More About the RPC</h4>
					<ul>
						<li><a href="/history/">History of the RPCGB</a></li>
						<li><a href="/services/">Services</a></li>
						<li><a href="/board-of-directors/">Board of Directors</a></li>
						<li><a href="/committees/">Committees</a></li>
						<li><a href="/resources/">Resources</a></li>
						<li><a href="/faq/">FAQ</a></li>
						<li><a href="/doing-business/">Doing Business</a></li>
						<li><a href="/staff/">Staff</a></li>
						<li><a href="/connections/">Connections</a></li>
						<li><a href="/requests-for-proposals-qualifications/">Requests for Proposals/Qualifications</a></li>
					</ul>
	         	</div>
         	</aside>
      	</div>
    	<div class="medium-9 columns">
			<article class="main-content">

			<?php if ( is_page (1114) ) { ?>
			<!-- Committee Meeting Minutes-->
				<div class="row">
					<div class="medium-6 columns">
						<h4>MPO Policy Committee Meeting Minutes</h4>
						<ul>
		               	<?php $minutes = new WP_Query( array( 'post_type' => 'minutes', 'category_name'  => 'mpo-policy', 'posts_per_page' => -1, 'order' => 'DSC' ) );
		               		while( $minutes->have_posts() ) : $minutes->the_post(); ?>
		                    	<li>
		                     		<a href="<?php echo types_render_field( "minutes-file", array( ) ) ?>" target="_blank">
		                        		<?php the_title(); ?>
		                     		</a>
		                     	</li>
		               		<?php endwhile; wp_reset_query(); ?>
		               	</ul>
					</div>
					<div class="medium-6 columns">
						<h4>Transportation Technical Committee Meeting Minutes</h4>
						<ul>
		               	<?php $minutes = new WP_Query( array( 'post_type' => 'minutes', 'category_name'  => 'technical', 'posts_per_page' => -1, 'order' => 'DSC' ) );
		               		while( $minutes->have_posts() ) : $minutes->the_post(); ?>
		                  	
		                    	<li>
		                     		<a href="<?php echo types_render_field( "minutes-file", array( ) ) ?>" target="_blank">
		                        		<?php the_title(); ?>
		                     		</a>
		                     	</li>
		               		<?php endwhile; wp_reset_query(); ?>
		               	</ul>
					</div>
				</div>

				<div class="row">
					<div class="medium-6 columns">
						<h4>MPO Advisory Committee Meeting Minutes</h4>
						<ul>
		               	<?php $minutes = new WP_Query( array( 'post_type' => 'minutes', 'category_name'  => 'mpo-advisory', 'posts_per_page' => -1, 'order' => 'DSC' ) );
		               	while( $minutes->have_posts() ) : $minutes->the_post(); ?>
		                  	
		                    	<li>
		                     		<a href="<?php echo types_render_field( "minutes-file", array( ) ) ?>" target="_blank">
		                        		<?php the_title(); ?>
		                     		</a>
		                     	</li>
		                  	
		               	<?php endwhile; wp_reset_query(); ?>
		               	</ul>
					</div>
					<div class="medium-6 columns">
						<h4>Citizens Committee Meeting Minutes</h4>
						<ul>
		               	<?php $minutes = new WP_Query( array( 'post_type' => 'minutes', 'category_name'  => 'citizens', 'posts_per_page' => -1, 'order' => 'DSC' ) );
			               	while( $minutes->have_posts() ) : $minutes->the_post(); ?>
			                  	
			                    	<li>
			                     		<a href="<?php echo types_render_field( "minutes-file", array( ) ) ?>" target="_blank">
			                        		<?php the_title(); ?>
			                     		</a>
			                     	</li>
			               	<?php endwhile; wp_reset_query(); ?>
		               	</ul>
					</div>
				</div>

				<?php } elseif ( is_page ('board-of-directors') ) { ?>
						<div class="entry-content">
							<?php 
								$board = get_post( 59 );
			        			$content = $board->post_content;
								
								echo $content; 
							?>
						</div>
						<h4>Meeting Minutes</h4>
						<ul>
			               	<?php $minutes = new WP_Query( array( 'post_type' => 'minutes', 'category_name'  => 'board-minutes', 'posts_per_page' => -1, 'order' => 'DSC' ) );
				               	while( $minutes->have_posts() ) : $minutes->the_post(); ?>
				                  	
				                    	<li>
				                     		<a href="<?php echo types_render_field( "minutes-file", array( ) ) ?>" target="_blank">
				                        		<?php the_title(); ?>
				                     		</a>
				                     	</li>
				               	<?php endwhile; wp_reset_query(); ?>
			            </ul>

				<?php } else { ?>

				<?php } ?>
			</article>
		</div>
	</div>
</div>

<?php get_footer();