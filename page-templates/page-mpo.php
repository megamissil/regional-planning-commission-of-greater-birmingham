<?php
/*
Template Name: MPO
*/

 get_header(); ?>
 

<div id="page" role="main">
   <header>
      <h1 class="entry-title"><?php the_title(); ?></h1>
   </header>
   <div class="row">
      <div class="medium-3 columns">
         <div class="sub-featured-img">
            <?php
               if (types_render_field('video-url', array('output'=>'true'))) {
                  echo types_render_field('video-url');
               } elseif ( has_post_thumbnail() ) { 
                  the_post_thumbnail();
               } else {

               }
            ?>
         </div>
         <?php get_sidebar(); ?>
      </div>
      <div class="medium-9 columns">
         <article <?php post_class('main-content') ?> id="post-<?php the_ID(); ?>">
            <div class="entry-content">
               <?php the_content(); ?>
               
               
               <!-- MPO Policy Committee -->
               <?php echo types_render_field( "mpo-policy", array( ) ) ?>
               <h4>Meetings</h4>
               <p>The regular meeting of the Birmingham MPO will be held on the second Wednesday of each month, unless otherwise noted, 1:30 pm.</p>
               
               <?php $calendar = new WP_Query( array( 'post_type' => 'minutes-calendar', 'category_name'  => 'mpo-policy', 'posts_per_page' => 1, 'order' => 'ASC' ) );
               while( $calendar->have_posts() ) : $calendar->the_post(); ?>
                  <p><span class="text-red">Next Meeting</span>: <?php the_title(); ?> <?php echo types_render_field( "calendar-time", array( ) ) ?></p>

               <?php endwhile; wp_reset_query(); ?>

               <a href="/transportation-planning/metropolitan-planning-organization/meeting-notes/" class="button">Meeting Archive</a>

               
               <!-- MPO Advisory Committee -->
               <?php echo types_render_field( "mpo-advisory-committee", array( ) ) ?>
               <h4>Meetings</h4>
               <p>The regular meeting of the Birmingham MPO Advisory Committee will be held on the fourth Thursday of each month, unless otherwise noted, 1:30 pm.</p>

               <?php $calendar = new WP_Query( array( 'post_type' => 'minutes-calendar', 'category_name'  => 'mpo-advisory', 'posts_per_page' => 1, 'order' => 'ASC' ) );
               while( $calendar->have_posts() ) : $calendar->the_post(); ?>
                  <p><span class="text-red">Next Meeting</span>: <?php the_title(); ?> <?php echo types_render_field( "calendar-time", array( ) ) ?></p>
               <?php endwhile; wp_reset_query(); ?>

               <a href="/transportation-planning/metropolitan-planning-organization/meeting-notes/" class="button">Meeting Archives</a>

               
               <!-- Transportation Technical Committee -->
               <?php echo types_render_field( "transportation-technical-committee", array( ) ) ?>
               <h4>Meetings</h4>
               <p>The regular meeting of the Transportation Technical Committee will be held on the fourth Wednesday of each month, unless otherwise noted, 10:00 am. Subcommittee meetings will be held on a call basis.</p>

               <?php $calendar = new WP_Query( array( 'post_type' => 'minutes-calendar', 'category_name'  => 'technical', 'posts_per_page' => 1, 'order' => 'ASC' ) );
               while( $calendar->have_posts() ) : $calendar->the_post(); ?>
                  <p><span class="text-red">Next Meeting</span>: <?php the_title(); ?> <?php echo types_render_field( "calendar-time", array( ) ) ?></p>
               <?php endwhile; wp_reset_query(); ?>

               <a href="/transportation-planning/metropolitan-planning-organization/meeting-notes/" class="button">Meeting Archives</a>
               

               <!-- Transportation Citizens Committee -->
               <?php echo types_render_field( "transportation-citizens-committee", array( ) ) ?>
               <h4>Meetings</h4>
               <p>The regular meeting of the Transportation Citizens Committee will be held on the third Wednesday of each month, unless otherwise noted, 12:00 pm. Subcommittee meetings will be held on a call basis.</p>

               <?php $calendar = new WP_Query( array( 'post_type' => 'minutes-calendar', 'category_name'  => 'citizens', 'posts_per_page' => 1, 'order' => 'ASC' ) );
               while( $calendar->have_posts() ) : $calendar->the_post(); ?>
                  <p><span class="text-red">Next Meeting</span>: <?php the_title(); ?> <?php echo types_render_field( "calendar-time", array( ) ) ?></p>
               <?php endwhile; wp_reset_query(); ?>

               <a href="/transportation-planning/metropolitan-planning-organization/meeting-notes/" class="button">Meeting Archives</a>
            </div>
         </article>
      </div>
   </div>



 <?php do_action( 'foundationpress_after_content' ); ?>


 </div>

 <?php get_footer();