<?php
/*
Template Name: Home
*/
get_header(); ?>

<section class="home-slider-container">
  	<div class="home-slider">
     	<?php $slider = new WP_Query( array( 'post_type' => 'home-slider', 'posts_per_page' => -1 ) );
     	while( $slider->have_posts() ) : $slider->the_post(); ?>
       		<div id="post-<?php the_ID(); ?>">
          		<?php echo types_render_field( "home-slide-image", array( "alt" => "home-slider-image" ) ) ?>
          		<div class="slider-caption">
			     	<h2><?php echo types_render_field( "home-slide-title", array( "alt" => "home-slider-image" ) ) ?></h2>
			     	<p><?php echo types_render_field( "home-slide-description", array( "alt" => "home-slider-image" ) ) ?></p>
			  	</div>
       		</div>
     	<?php endwhile; wp_reset_query(); ?>
  	</div>
</section>

<section class="home-content">
    <div class="home-why-rpc">
        <div class="home-why-rpc-img">
			<?=types_render_field('video-url'); ?>
        </div>
        <div class="home-why-rpc-info">
			<h2><?php the_title(); ?></h2>
            <?php the_content(); ?>
        </div>
    </div>
    <div class="home-callouts" data-equalizer data-equalize-on="medium">
		<div class="home-callout">
			<div class="home-callout-img">
				<img src="<?php echo get_stylesheet_directory_uri(); ?>/assets/images/icon-news.png" alt="">
			</div>
			<div class="home-callout-title">
				<h4>Latest News</h4>
			</div>
			<hr>
			<ul class="home-callout-list" data-equalizer-watch>
	            <?php $news = new WP_Query( array( 'posts_per_page' => 4 ) );
	            while( $news->have_posts() ) : $news->the_post(); ?>
					<li><a href="<?php the_permalink(); ?>"><?php the_title(); ?></a></li>
				<?php endwhile; ?>
			</ul>
			<a href="/news/" class="button">View All News</a>
		</div>
		<div class="home-callout">
			<div class="home-callout-img">
				<img src="<?php echo get_stylesheet_directory_uri(); ?>/assets/images/icon-events.png" alt="">
			</div>
			<div class="home-callout-title">
				<h4>Calendar</h4>
			</div>
			<hr>
			
			<ul class="home-callout-list" data-equalizer-watch>
				<p>For upcoming MPO meetings, <a href="/mpo-meetings/">click here</a>.</p>
				<?php get_template_part( 'template-parts/home-calendar-categories' ); ?>
			</ul>
			<a href="/calendar/" class="button">View Calendar</a>
		</div>
		<div class="home-callout">
			<div class="home-callout-img">
				<img src="<?php echo get_stylesheet_directory_uri(); ?>/assets/images/icon-subscribe.png" alt="">
			</div>
			<div class="home-callout-title">
				<h4>Subscribe</h4>
			</div>
			<hr>
			<p>Be the first to know about what we're doing for the Magic City!</p>
		</div>
    </div>
</section>

<?php get_footer();