<?php
/*
Template Name: Glossary
*/

get_header(); ?>
 

<div id="page" role="main">
   <header>
      <h1 class="entry-title"><?php the_title(); ?></h1>
   </header>
   <div class="row">
      <div class="medium-3 columns">
         <div class="sub-featured-img">
            <?php
               if ( has_post_thumbnail() ) :
                  the_post_thumbnail();
               endif;
            ?>
         </div>
         <?php get_sidebar(); ?>
      </div>
      <div class="medium-9 columns">
         <article <?php post_class('main-content') ?> id="post-<?php the_ID(); ?>">
            <div class="entry-content">
               <?php 
                  $args = array(
                     'post_type' => 'glossary-term',
                     'showposts' => -1
                  );
                  $glossary = new WP_Query( $args );

                  while( $glossary->have_posts() ) : $glossary->the_post(); 
               ?>
                  <ul class="accordion" data-accordion data-allow-all-closed="true">
                     <li class="accordion-item" data-accordion-item>
                        <a class="accordion-title">
                           <?php echo types_render_field( "gloss-term", array( ) ) ?>
                        </a>
                        <div class="accordion-content" data-tab-content>
                           <p><?php echo types_render_field( "gloss-definition", array( ) ) ?></p>
                        </div>
                     </li>
                  </ul>
               <?php endwhile; ?>
                                             
               <?php wp_reset_postdata(); ?>
            </div>
         </article>
      </div>
   </div>



 <?php do_action( 'foundationpress_after_content' ); ?>


 </div>

 <?php get_footer();
