<?php

//foreach ($categories as $index => $category):

    $posts = get_posts(array(
      'post_type' => 'meeting-calendar',
      'post_status' => 'publish',
      'orderby' => 'date',
      'order' => 'ASC',
      'posts_per_page' => 4,
      // 'tax_query' => array(
      //   array(
      //     'taxonomy' => 'category',
      //     'field'    => 'slug',
      //     'terms'    => 'category_name'
      //   )
      // )
    ));

    $query = new WP_Query( $posts ); ?>


        <?php foreach ($posts as $post): get_post($post); ?>
            <p><?php echo '<span>' . the_category(' ') . '</span>'; ?></p>
            <div class="row">
              <div class="medium-6 columns">
                  <?php the_title(); ?>
              </div>
              <div class="medium-6 columns">
                  <?php echo types_render_field( "calendar-time", array( ) ) ?>
              </div>
            </div>
        <?php endforeach; ?>

<?php// endforeach; ?>