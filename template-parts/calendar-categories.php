

<div class="cal-head">
    <div class="small-4 columns">
        <h4>Date</h4>
    </div>
    <div class="small-3 columns">
        <h4>Time</h4>
    </div>
    <div class="small-5 columns">
        <h4>Type</h4>
    </div>
</div>

<?php
// Retrieve taxonomies related to this post
$categories = get_categories();

foreach ($categories as $index => $category):
  // if ($category->parent !== 0):

    $posts = get_posts(array(
      'post_type' => 'meeting-calendar',
      'post_status' => 'publish',
      'orderby' => 'date',
      'order' => 'ASC',
      'posts_per_page' => -1,
      'tax_query' => array(
        array(
          'taxonomy' => 'category',
          'field'    => 'slug',
          'terms'    => $category->slug
        )
      )
    ));

    $query = new WP_Query( $posts ); ?>


    
        <?php foreach ($posts as $post): get_post($post); ?>
            <div class="cal-item">
                <div class="medium-4 columns">
                    <?php the_title(); ?>
                </div>
                <div class="medium-3 columns">
                    <?php echo types_render_field( "calendar-time", array( ) ) ?>
                </div>
                <div class="medium-5 columns">
                    <?php echo '<span>' . $category->name . '</span>'; ?>
                </div>
            </div>
        <?php endforeach; ?>
    
    <?php //endif; ?>
<?php endforeach; ?>


<!-- <div class="row collapse">
  <div class="medium-3 columns">
    <ul class="tabs vertical" id="example-vert-tabs" data-tabs>
      <li class="tabs-title is-active"><a href="#panel1v" aria-selected="true">All</a></li>
      <li class="tabs-title"><a href="#panel2v">Board of Directors</a></li>
      <li class="tabs-title"><a href="#panel3v">MPO Advisory</a></li>
      <li class="tabs-title"><a href="#panel4v">MPO Policy</a></li>
      <li class="tabs-title"><a href="#panel5v">Transportation Citizens</a></li>
      <li class="tabs-title"><a href="#panel6v">Transportation Technical</a></li>
      <li class="tabs-title"><a href="#panel6v">Events</a></li>
    </ul>
    </div>
    <div class="medium-9 columns">
    <div class="tabs-content vertical" data-tabs-content="example-vert-tabs">
      <div class="tabs-panel is-active" id="panel1v">
        <p>Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua.</p>
      </div>
      <div class="tabs-panel" id="panel2v">
        <p>Vivamus hendrerit arcu sed erat molestie vehicula. Sed auctor neque eu tellus rhoncus ut eleifend nibh porttitor. Ut in nulla enim. Phasellus molestie magna non est bibendum non venenatis nisl tempor. Suspendisse dictum feugiat nisl ut dapibus.</p>
      </div>
      <div class="tabs-panel" id="panel3v">
        <img class="thumbnail" src="assets/img/generic/rectangle-3.jpg">
      </div>
      <div class="tabs-panel" id="panel4v">
        <p>Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua.</p>
      </div>
      <div class="tabs-panel" id="panel5v">
        <p>Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est laborum.</p>
      </div>
      <div class="tabs-panel" id="panel6v">
        <img class="thumbnail" src="assets/img/generic/rectangle-5.jpg">
      </div>
    </div>
  </div>
</div> -->