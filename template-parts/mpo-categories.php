

<!-- Sorry -->
<ul class="accordion mpo-accordion" data-accordion data-allow-all-closed="true">
    <li class="accordion-item" data-accordion-item>
        <a class="accordion-title">
            MPO Advisory Committee Meeting
        </a>
        <div class="accordion-content" data-tab-content>
			<?php 
				$args = array(
				 	'post_type' => 'meeting-calendar',
				 	'post_status' => 'publish',
					'orderby' => 'date',
				 	'category_name'  => 'mpo-advisory', 
				 	'posts_per_page' => 1, 
				 	'order' => 'DESC'
				); 

				$advisory = new WP_Query( $args );

				while( $advisory->have_posts() ) : $advisory->the_post(); 
			?>

        	<div class="row">
				<div class="medium-4 columns">
					<h4>Next Meeting</h4>
					<p class="text-red"><strong><?php the_title(); ?></strong><br>
            		<?php echo types_render_field( "calendar-time", array( ) ) ?></p>
				</div>
				<div class="medium-4 columns">
					<h4>Supporting Documents</h4>
					<?php if (types_render_field('document-file', array('output'=>'true'))) { ?>
						<ul>
	                        <li>
	                           <a href="<?php echo types_render_field( "document-file", array( ) ) ?>" target="_blank">
	                              <?php echo types_render_field( "document-title", array( ) ) ?>
	                           </a>
	                        </li>
						</ul>
					<?php } ?>
				</div>
				<div class="medium-4 columns">
					<h4>Meeting Archive</h4>
					<ul>
	                  <?php $minutes = new WP_Query( array( 'post_type' => 'minutes', 'posts_per_page' => 5, 'category_name'  => 'mpo-advisory', 'order' => 'DESC' ) );
	                     while( $minutes->have_posts() ) : $minutes->the_post(); ?>
	                        <li>
	                           <a href="<?php echo types_render_field( "minutes-file", array( ) ) ?>" target="_blank">
	                              <?php the_title(); ?>
	                           </a>
	                        </li>
	                  <?php endwhile; wp_reset_query(); ?>
	               </ul>
	               <a href="/transportation-planning/metropolitan-planning-organization/meeting-notes/" class="button">Meeting Archives</a>
				</div>
        	</div>
        	<?php endwhile; wp_reset_query(); ?>
        </div>
    </li>

    <li class="accordion-item" data-accordion-item>
        <a class="accordion-title">
            MPO Citizens Committee Meeting
        </a>
        <div class="accordion-content" data-tab-content>
        	<?php 
				$args = array(
				 	'post_type' => 'meeting-calendar',
				 	'post_status' => 'publish',
					'orderby' => 'date',
				 	'category_name'  => 'citizens', 
				 	'posts_per_page' => 1, 
				 	'order' => 'DESC'
				); 

				$citizens = new WP_Query( $args );

				while( $citizens->have_posts() ) : $citizens->the_post(); 
			?>
        	<div class="row">
				<div class="medium-4 columns">
					<h4>Next Meeting</h4>
					<p class="text-red"><strong><?php the_title(); ?></strong><br>
            		<?php echo types_render_field( "calendar-time", array( ) ) ?></p>
				</div>
				<div class="medium-4 columns">
					<h4>Supporting Documents</h4>
					<?php if (types_render_field('document-file', array('output'=>'true'))) { ?>
						<ul>
	                        <li>
	                           <a href="<?php echo types_render_field( "document-file", array( ) ) ?>" target="_blank">
	                              <?php echo types_render_field( "document-title", array( ) ) ?>
	                           </a>
	                        </li>
						</ul>
					<?php } ?>
				</div>
				<div class="medium-4 columns">
					<h4>Meeting Archive</h4>
					<ul>
	                  <?php $minutes = new WP_Query( array( 'post_type' => 'minutes', 'posts_per_page' => 5, 'category_name'  => 'citizens', 'order' => 'DSC' ) );
	                     while( $minutes->have_posts() ) : $minutes->the_post(); ?>
	                        <li>
	                           <a href="<?php echo types_render_field( "minutes-file", array( ) ) ?>" target="_blank">
	                              <?php the_title(); ?>
	                           </a>
	                        </li>
	                  <?php endwhile; wp_reset_query(); ?>
	               </ul>
	               <a href="/transportation-planning/metropolitan-planning-organization/meeting-notes/" class="button">Meeting Archives</a>
				</div>
        	</div>
        	<?php endwhile; wp_reset_query(); ?>
        </div>
    </li>

    <li class="accordion-item" data-accordion-item>
        <a class="accordion-title">
            MPO Policy Committee Meeting
        </a>
        <div class="accordion-content" data-tab-content>
        	<?php 
				$args = array(
				 	'post_type' => 'meeting-calendar',
				 	'post_status' => 'publish',
					'orderby' => 'date', 
				 	'category_name'  => 'mpo-policy', 
				 	'posts_per_page' => 1, 
				 	'order' => 'DESC'
				); 

				$policy = new WP_Query( $args );

				while( $policy->have_posts() ) : $policy->the_post(); 
			?>
        	<div class="row">
				<div class="medium-4 columns">
					<h4>Next Meeting</h4>
					<p class="text-red"><strong><?php the_title(); ?></strong><br>
            		<?php echo types_render_field( "calendar-time", array( ) ) ?></p>
				</div>
				<div class="medium-4 columns">
					<h4>Supporting Documents</h4>
					<?php if (types_render_field('document-file', array('output'=>'true'))) { ?>
						<ul>
	                        <li>
	                           <a href="<?php echo types_render_field( "document-file", array( ) ) ?>" target="_blank">
	                              <?php echo types_render_field( "document-title", array( ) ) ?>
	                           </a>
	                        </li>
						</ul>
					<?php } ?>
				</div>
				<div class="medium-4 columns">
					<h4>Meeting Archive</h4>
					<ul>
	                  <?php $minutes = new WP_Query( array( 'post_type' => 'minutes', 'posts_per_page' => 5, 'category_name'  => 'mpo-policy', 'order' => 'DSC' ) );
	                     while( $minutes->have_posts() ) : $minutes->the_post(); ?>
	                        <li>
	                           <a href="<?php echo types_render_field( "minutes-file", array( ) ) ?>" target="_blank">
	                              <?php the_title(); ?>
	                           </a>
	                        </li>
	                  <?php endwhile; wp_reset_query(); ?>
	               </ul>
	               <a href="/transportation-planning/metropolitan-planning-organization/meeting-notes/" class="button">Meeting Archives</a>
				</div>
        	</div>
        <?php endwhile; wp_reset_query(); ?>
        </div>
    </li>

    <li class="accordion-item" data-accordion-item>
        <a class="accordion-title">
            	MPO Technical Committee Meeting
        </a>
        <div class="accordion-content" data-tab-content>
        	<?php 
				$args = array(
				 	'post_type' => 'meeting-calendar',
				 	'post_status' => 'publish',
					'orderby' => 'date',
				 	'category_name'  => 'technical', 
				 	'posts_per_page' => 1, 
				 	'order' => 'DESC'
				); 

				$technical = new WP_Query( $args );

				while( $technical->have_posts() ) : $technical->the_post(); 
			?>
        	<div class="row">
				<div class="medium-4 columns">
					<h4>Next Meeting</h4>
					<p class="text-red"><strong><?php the_title(); ?></strong><br>
            		<?php echo types_render_field( "calendar-time", array( ) ) ?></p>
				</div>
				<div class="medium-4 columns">
					<h4>Supporting Documents</h4>
					<?php if (types_render_field('document-file', array('output'=>'true'))) { ?>
						<ul>
	                        <li>
	                           <a href="<?php echo types_render_field( "document-file", array( ) ) ?>" target="_blank">
	                              <?php echo types_render_field( "document-title", array( ) ) ?>
	                           </a>
	                        </li>
						</ul>
					<?php } ?>
				</div>
				<div class="medium-4 columns">
					<h4>Meeting Archive</h4>
					<ul>
	                  <?php $minutes = new WP_Query( array( 'post_type' => 'minutes', 'posts_per_page' => 5, 'category_name'  => 'technical', 'order' => 'DSC' ) );
	                     while( $minutes->have_posts() ) : $minutes->the_post(); ?>
	                        <li>
	                           <a href="<?php echo types_render_field( "minutes-file", array( ) ) ?>" target="_blank">
	                              <?php the_title(); ?>
	                           </a>
	                        </li>
	                  <?php endwhile; wp_reset_query(); ?>
	               </ul>
	               <a href="/transportation-planning/metropolitan-planning-organization/meeting-notes/" class="button">Meeting Archives</a>
				</div>
        	</div>
        <?php endwhile; wp_reset_query(); ?>
        </div>
    </li>
</ul>