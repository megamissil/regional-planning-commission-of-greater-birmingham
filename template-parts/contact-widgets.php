<div class="map-row">
 	<div class="address-col">
    	<?php dynamic_sidebar( 'contact-address-widget' ); ?>
 	</div>
 	<div class="map-col">
    	<?php dynamic_sidebar( 'contact-map-widget' ); ?>
 	</div>
</div>

<div class="contact-form">
	<?php echo do_shortcode('[contact-form-7 id="24" title="Contact form 1"]'); ?>
</div>