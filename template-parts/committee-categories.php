<?php

// Retrieve the post's (page, technically) meta data
//$post = get_post();

// Retrieve taxonomies related to this post
$categories = get_categories();

foreach ($categories as $index => $category):
	if ($category->parent !== 0):

		$posts = get_posts(array(
			'post_type' => 'meeting-calendar',
			'post_status' => 'publish',
			'orderby' => 'date',
			'order' => 'DESC',
			'showposts' => 1,
			'tax_query' => array(
				array(
					'taxonomy' => 'category',
					'field'    => 'slug',
					'terms'    => $category->slug
				)
			)
		));

		$query = new WP_Query( $posts );

			foreach ($posts as $post): get_post($post); ?>
				<ul class="accordion" data-accordion data-allow-all-closed="true">
	                <li class="accordion-item" data-accordion-item>
	                    <a class="accordion-title">
	                        <?php echo '<span>' . $category->name . ' Meeting</span>'; ?>
	                    </a>
	                    <div class="accordion-content" data-tab-content>
	                    	<div class="row">
								<div class="medium-4 columns">
									<h4><?php the_title(); ?></h4>
	                        		<p><?php echo types_render_field( "calendar-time", array( ) ) ?></p>
								</div>
								<div class="medium-4 columns">
									<h4>Supporting Documents</h4>
									<?php /*$documents = new WP_Query( array( 'post_type' => 'supporting-document', 'posts_per_page' => -1, 'order' => 'DSC' ) );
				                     while( $documents->have_posts() ) : $documents->the_post(); ?>
				                        <li>
				                           <a href="<?php echo types_render_field( "document-file", array( ) ) ?>" target="_blank">
				                              <?php echo types_render_field( "document-title", array( ) ) ?>
				                           </a>
				                        </li>
				                  <?php endwhile; wp_reset_query();*/ ?>
					               	<ul>
					                    <li>
					                        <a href="<?php echo types_render_field( "document-file", array( ) ) ?>" target="_blank">
					                            <?php echo types_render_field( "document-title", array( ) ) ?>
					                        </a>
					                    </li>
					               	</ul>
								</div>
								<div class="medium-4 columns">
									<h4>Meeting Archive</h4>
									<ul>
					                  <?php $minutes = new WP_Query( array( 'post_type' => 'minutes', 'posts_per_page' => 5, 'order' => 'DSC' ) );
					                     while( $minutes->have_posts() ) : $minutes->the_post(); ?>
					                        <li>
					                           <a href="<?php echo types_render_field( "minutes-file", array( ) ) ?>" target="_blank">
					                              <?php the_title(); ?>
					                           </a>
					                        </li>
					                  <?php endwhile; wp_reset_query(); ?>
					               </ul>
					               <a href="/transportation-planning/metropolitan-planning-organization/meeting-notes/" class="button">Meeting Archives</a>
								</div>
	                    	</div>
	                    </div>
	                </li>
	            </ul>
            <?php endforeach; ?>
	<?php endif; ?>
<?php endforeach; ?>