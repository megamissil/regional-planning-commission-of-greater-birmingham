<?php
/**
 * The template for displaying the footer
 *
 * Contains the closing of the "off-canvas-wrap" div and all content after.
 *
 * @package FoundationPress
 * @since FoundationPress 1.0.0
 */

?>

		</section>
		<div id="footer-container">
			<section class="top-footer">
				<div class="row">
					<div class="ft-map">
						<div class="row">
							<div class="medium-6 columns">
								<iframe src="https://www.google.com/maps/embed?pb=!1m18!1m12!1m3!1d3326.5374239341995!2d-86.80780918446953!3d33.51341025337889!2m3!1f0!2f0!3f0!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x88891b9441b4b521%3A0x6f25166dc775446f!2s2+20th+St+N%2C+Birmingham%2C+AL+35203!5e0!3m2!1sen!2sus!4v1472074658091" width="100%" height="300" frameborder="0" style="border:0" allowfullscreen></iframe>
							</div>
							<div class="medium-6 columns">
								<h4>Regional Planning Commission of Greater Birmingham</h4>
								<p>2 20th Street North, Suite 1200<br>
								Birmingham, AL 35203<br>
								Phone: 205.251.8139</p>
							</div>
						</div>
					</div>
					<div class="ft-twitter">
						<a class="twitter-timeline" data-height="300" data-link-color="#c60e3b" href="https://twitter.com/RPCGB">Tweets by RPCGB</a> <script async src="//platform.twitter.com/widgets.js" charset="utf-8"></script>
					</div>
				</div>
			</section>
			<section class="bottom-footer">
				<div class="row">
					<div class="ft-copy">
						<p><a href="/doing-business/">Doing Business</a> | <a href="/employment/">Employment</a></p>
						<p>Copyright &copy; <?=date('Y'); ?> Regional Planning Commission of Greater Birmingham</p>
					</div>
					<div class="ft-moxy">
						<span class="moxy"><a href="http://digmoxy.com" target="_blank">Moxy</a></span>
					</div>
				</div>
			</section>
		</div>

		<?php do_action( 'foundationpress_layout_end' ); ?>

<?php if ( get_theme_mod( 'wpt_mobile_menu_layout' ) == 'offcanvas' ) : ?>
		</div><!-- Close off-canvas wrapper inner -->
	</div><!-- Close off-canvas wrapper -->
</div><!-- Close off-canvas content wrapper -->
<?php endif; ?>

<script type="text/javascript" src="<?php echo get_stylesheet_directory_uri(); ?>/assets/components/slick/slick.min.js"></script>
<?php wp_footer(); ?>
<?php do_action( 'foundationpress_before_closing_body' ); ?>
</body>
</html>
